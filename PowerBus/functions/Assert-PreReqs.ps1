﻿function Assert-PreReqs {
[cmdletbinding()]
[OutPutType([bool])]
Param(
    [string[]]$files
)
    $F = $MyInvocation.MyCommand.Name
    Write-Verbose "$PSScriptRoot"
    Write-Verbose -Message "$F - START"
    $returnValue = $true
    if(-not $files)
    {
        $returnValue = $false
    }
    foreach($file in $files)
    {
        Write-Verbose -Message "Checking file $file"
        $returnValue = $returnValue -and (Test-Path -Path $file)
    }
    $returnValue
    Write-Verbose -Message "$f - END"
}
