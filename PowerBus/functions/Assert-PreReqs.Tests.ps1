﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

Describe "Assert-PreReqs" {
    Context "Parameter validation" {
        It "Valid path provided should be true" {
            Assert-PreReqs -files "$PSScriptRoot\$($MyInvocation.MyCommand.Name)" | Should Be $true
        }

        It "Invalid path provided should be false" {
            Assert-PreReqs -files "$PSScriptRoot\$($MyInvocation.MyCommand.Name)1" | Should Be $false
        }

        It "Valid path provided should not throw" {
            { Assert-PreReqs -files "$PSScriptRoot\$($MyInvocation.MyCommand.Name)" } | Should not throw
        }

        It "No parameters should be false" {
            Assert-PreReqs | Should Be $false
        }
    }
}
